# syndicate-sh

This is an implementation of the Syndicate network protocol (based on
[Preserves](https://preserves.dev/)) for Bash.

**Q.** Is this a joke, or is this serious?  
**A.** Yes.

## Dependencies

First, it depends on Bash-specific shell features.

Second, you'll need a Syndicate network server ("broker"). The best
option at present is the Rust-language server. Unfortunately it relies
on a few nightly-only features of Rust, so I use `rustup` to get a
nightly toolchain, and then use that:

    # compiling syndicate-server from source needs these things:
    apt install libssl-dev pkg-config curl build-essential

    # this is the command-line given by https://rustup.rs/:
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    # follow the instructions
    # then log out and in again

    # now you can get the nightly toolchain:
    rustup toolchain install nightly

    # and finally the server:
    cargo +nightly install syndicate-server

Third, you'll need the `preserves-tool` program, which `syndicate.sh`
uses to slice and dice network packets. It's also written in Rust (but
doesn't require nightly), so you can get it with:

    cargo install preserves-tools

## Running the demo

Start the server in one terminal, in the same directory as this
`README.md` file:

    syndicate-server -s ./sock

Then, in one or more other terminals, run `chat.sh` from the same
directory:

    ./examples/chat.sh
