#!/bin/bash

set -euo pipefail

. $(dirname "$0")/../lib/syndicate.sh

demo_ds_resolved() {
    local oid="$(ds_project "$ds_event" '^ A . 0 ^ accepted . 0 .embedded . 1')"
    if [ -n "$oid" ]
    then
        ds_ref="#:[1 $oid]"
        echo "Dataspace: $ds_ref"

        username="user$$"

        declare -A demo_user_presence
        demo_presence() {
            case $(ds_project "$ds_event" .^) in
                A)
                    local h="$(ds_project "$ds_event" '. 1')"
                    local who="$(ds_project "$ds_event" '. 0 . 0')"
                    demo_user_presence[$h]="$who"
                    echo "JOIN: $who"
                    ;;
                R)
                    local h="$(ds_project "$ds_event" '. 0')"
                    local who="${demo_user_presence[$h]}"
                    echo "PART: $who"
                    unset 'demo_user_presence[$h]'
                    ;;
            esac
        }
        ds_object presence demo_presence
        ds_assert "$ds_ref" "<Observe <rec Present [<bind <_>>]> $presence>"
        ds_assert "$ds_ref" "<Present \"$username\">"

        demo_utterance() {
            local who="$(ds_project "$ds_event" '^ M . 0 . 0')"
            local what="$(ds_project "$ds_event" '^ M . 0 . 1')"
            echo "$who: $what"
        }
        ds_object utterance demo_utterance
        ds_assert "$ds_ref" "<Observe <rec Says [<bind <_>> <bind <_>>]> $utterance>"

        ds_flush
        ds_mainloop </dev/null &
        mainloop_pid=$!
        while read line
        do
            ds_message "$ds_ref" "<Says \"$username\" \"$line\">"
            ds_flush
        done </dev/tty
        kill $mainloop_pid
        exit 0
    fi
}

ds_object bootk demo_ds_resolved
ds_connect \
    '<unix "./sock">' \
    'ds_assert "#:[1 0]" "<resolve <ref {oid: \"syndicate\" sig: #x\"69ca300c1dbfa08fba692102dd82311a\"}> $bootk>"'
